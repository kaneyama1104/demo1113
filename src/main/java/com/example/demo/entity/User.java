//package com.example.demo.entity;
//
//import java.time.LocalDateTime;
//
//public class User
//{
//	private int id;
//	private String name;
//	private String email;
//	private String contents;
//	private LocalDateTime created;
//	
//	public User()
//	{
//		
//	}
//	
//	public User(int id, String name, String email, String contents, LocalDateTime created)
//	{
//		this.id = id;
//		this.name = name;
//		this.email = email;
//		this.contents = contents;
//		this.created = created;
//	}
//
//	public int getId()
//	{
//		return id;
//	}
//	public void setId(int id)
//	{
//		this.id = id;
//	}
//	public String getName()
//	{
//		return name;
//	}
//	public void setName(String name)
//	{
//		this.name = name;
//	}
//	public String getEmail()
//	{
//		return email;
//	}
//	public void setEmail(String email)
//	{
//		this.email = email;
//	}
//	public String getContents()
//	{
//		return contents;
//	}
//	public void setContents(String contents)
//	{
//		this.contents = contents;
//	}
//	public LocalDateTime getCreated()
//	{
//		return created;
//	}
//	public void setCreated(LocalDateTime created)
//	{
//		this.created = created;
//	}
//	
//}
package com.example.demo.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//DB用クラス
@Component
@SessionScope
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false, unique = true)
	private String email;

	@Column(nullable = false)
	private String password;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "users_roles", joinColumns = {
			@JoinColumn(name = "USER_ID", referencedColumnName = "ID") }, inverseJoinColumns = {
					@JoinColumn(name = "ROLE_ID", referencedColumnName = "ID") })
	private List<Role> roles = new ArrayList<>();

}
