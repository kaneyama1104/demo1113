package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.dao.UserDao;
import com.example.demo.entity.User;
import com.example.demo.form.UserForm;
import com.example.demo.service.UserService;

//@Controller
//@RequestMapping("/form")
public class UserController {
	@Autowired
	private UserService userService;

	@GetMapping
	public String index(Model model) {
		List<UserDao> list = userService.getAllUsers();
		model.addAttribute("userList", list);
		model.addAttribute("title", "Index");
		return "index";
	}

	@GetMapping("/input")
	public String get_input(UserForm user, Model model, @ModelAttribute("complete") String complete_value) {
		model.addAttribute("title", "input form[get]");
		return "forms/input_form";
	}

	@PostMapping("/input")
	public String post_input(UserForm user, Model model) {
		model.addAttribute("title", "input form[get]");
		return "forms/input_form";
	}

	@PostMapping("/confirm")
	public String confirm(@Validated UserForm user, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("title", "input form[Go Back Form Confirm");
			return "forms/input_form";
		}
		model.addAttribute("title", "confirm form");
		return "forms/confirm";
	}

	@PostMapping("/complete")
	public String complete(@Validated UserForm userForm, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			model.addAttribute("title", "input form(Go Back From Complete");
			return "forms/input_form";
		}
		User user1 = new User();
		user1.setName(userForm.getName());
		user1.setEmail(userForm.getEmail());
		// user1.setContents(userForm.getContents());
		// user1.setCreated(LocalDateTime.now());
		int i = userService.addUserToMysql(user1);
		if (i > 0)
			System.out.println("insert ok");
		else
			System.out.println("insert failed");
		redirectAttributes.addFlashAttribute("complete", "Registered");
		return "redirect:/form/input";
	}
}
