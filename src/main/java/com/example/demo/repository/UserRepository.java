package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.User;

//Controller から実装時に、直接呼び出し用
public interface UserRepository extends JpaRepository<User, Long> {
	User findByEmail(String email);
}
