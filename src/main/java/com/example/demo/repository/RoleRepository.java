package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Role;

//Controller から実装時に、直接呼び出し用
public interface RoleRepository extends JpaRepository<Role, Long> {
	Role findByName(String name);
}
