package com.example.demo.dao;

import java.util.List;

import com.example.demo.entity.User;

public interface UserDao
{
	public abstract List<UserDao> selectAllUsers();
	public abstract int insertUser(User user);
}
