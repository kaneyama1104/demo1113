package com.example.demo.service;

import java.util.List;

import com.example.demo.dao.UserDao;
import com.example.demo.dao.UserDto;
import com.example.demo.entity.User;
import com.example.demo.model.UserModel;

//Controller から実装時に、直接呼び出し用
public interface UserService {
	public abstract List<UserDao> getAllUsers();

	public abstract int addUserToMysql(User user);

	public abstract void save(UserDto userDto);

	public abstract UserModel findByUsername(String username);

}
