package com.example.demo.service;

import java.util.List;

import com.example.demo.dao.UserDto;
import com.example.demo.entity.User;

//Controller から実装時に、直接呼び出し用
public interface UserServiceNew {

	void saveUser(UserDto userDto);

	User findByEmail(String email);

	List<UserDto> findAllUsers();

	// ADD KANEYAMA 2023/11/19
	List<UserDto> findByEmailUserDto(String email);

}
