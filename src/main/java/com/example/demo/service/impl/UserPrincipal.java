package com.example.demo.service.impl;

import com.example.demo.entity.User;
//Userクラスを使うためにインポートしています
import com.example.demo.model.UserModel;

//UserDetailsインターフェースを実装したUserPrincipalというクラスを作成します。これはSpring Securityでユーザー情報を扱うためのクラスです。
public class UserPrincipal {

	private UserModel user; // Userオブジェクトを保持します。

	// コンストラクタでUserオブジェクトを受け取り、それをこのクラスのuserにセットします。
	public UserPrincipal(UserModel user) {
		this.user = user;
	}

	public UserModel findByUsername(String username) {
		return null;
	}

	public User findByEmail(String username) {
		return null;
	}

	public void save(UserModel user) {

	}

//
//	// ユーザーに与えられる権限を返します。ここでは全てのユーザーに"USER"という権限を与えています。
//	@Override
//	public Collection<? extends GrantedAuthority> getAuthorities() {
//		return Collections.singleton(new SimpleGrantedAuthority("USER"));
//	}
//
//	// Userオブジェクトのパスワードを返します。
//	@Override
//	public String getPassword() {
//		return user.getPassword();
//	}
//
//	// Userオブジェクトのユーザー名を返します。
//	@Override
//	public String getUsername() {
//		return user.getUsername();
//	}
//
//	// アカウントが有効期限切れでないことを示すために、常にtrueを返します。
//	@Override
//	public boolean isAccountNonExpired() {
//		return true;
//	}
//
//	// アカウントがロックされていないことを示すために、常にtrueを返します。
//	@Override
//	public boolean isAccountNonLocked() {
//		return true;
//	}
//
//	// 資格情報（ここではパスワード）が有効期限切れでないことを示すために、常にtrueを返します。
//	@Override
//	public boolean isCredentialsNonExpired() {
//		return true;
//	}
//
//	// アカウントが有効であることを示すために、常にtrueを返します。
//	@Override
//	public boolean isEnabled() {
//		return true;
//	}
}
