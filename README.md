### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

# Demo1113
## Mysql
#### mysql port
```m
  3306
```
#### CREATE DATABASE
```bash
  CREATE DATABASE udemy_userdb;
```
#### CREATE TABLE
```bash
  CREATE TABLE t_user
(
   id INT NOT NULL AUTO_INCREMENT,
   name VARCHAR(100) NOT NULL,
   email VARCHAR(100) NOT NULL,
   contents VARCHAR(500) NOT NULL,
   created DATETIME NOT NULL,
   PRIMARY KEY(id)
);
```
#### INSERT INTO TABLE
```bash
INSERT INTO t_user(name, email, contents, created)VALUES('Ethan', 'sample@example.com', 'Hello', '2019-11-12 08:34:19');
```
example:
| id | name |       email       |contents |      created      |
| :- | :--  | :-----------------|:--------|:------------------|
|  1 | Ethan| sample@example.com|  Hello  |2019-11-12 08:34:19|
## WebBrowser
Add a user:
```bash
  localhost:8080/form/input
```
List all users:
```bash
  localhost:8080/form
```
